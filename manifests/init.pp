#


class mcol {


}

class mcol::params {
        case $::osfamily {
		'Darwin':{
			$agent_cfg = "/private/etc/puppetlabs/mcollective/server.cfg"
			$client_cfg = "/private/etc/puppetlabs/mcollective/server.cfg"
			$agent_service = "mcollective"
			$mcol_modpath = "/usr/local/share/mcollective"
			$mcol_puppetlabs = "yes"

			$puppetcertpemdir = "/private/etc/puppetlabs/puppet/ssl/certs/"
			$puppetkeypemdir = "/private/etc/puppetlabs/puppet/ssl/private_keys"
		}
		'OpenBSD':{
			# mcol agent stuff
			$all_packages = [ 'mcollective' ]

			$puppetcertpemdir = "/etc/puppetlabs/puppet/ssl/certs"
			$puppetkeypemdir = "/etc/puppetlabs/puppet/ssl/private_keys"

			$agent_cfg = "/etc/mcollective/server.cfg"
			$client_cfg = "/etc/mcollective/client.cfg"
			$agent_service = "mcollectived"
			$mcol_modpath = "/usr/local/share/mcollective"

			$yamlfacts = "/etc/mcollective/facts.yaml"
			$mcol_libdir="/usr/local/share"


		}
                'FreeBSD':{
			$rabbit_dir = '/usr/local/etc/rabbitmq'
			$rabbit_user = "rabbitmq"

			$brokerts = '/usr/local/etc/activemq/mcolbroker.ts'
			$brokerks = '/usr/local/etc/activemq/mcolbroker.ks'

			$amq_packages = [
				'activemq'
			]
			$amq_config = '/usr/local/etc/activemq/activemq.xml'
			$amq_service = 'activemq'

			#
			# mcollective stuff
			#

			$all_packages = [ 'mcollective' ]
			

			$puppetcertpemdir = "/var/puppet/ssl/certs"
			$puppetkeypemdir = "/var/puppet/ssl/private_keys"

			$agent_cfg = "/usr/local/etc/mcollective/server.cfg"
			$client_cfg = "/usr/local/etc/mcollective/client.cfg"
			$agent_service = "mcollectived"
			$mcol_modpath = "/usr/local/share/mcollective"

			$yamlfacts = "/usr/local/etc/mcollective/facts.yaml"
			$mcol_libdir="/usr/local/share"

		}

		'Archlinux':{
			if $mcol_puppetlabs=='yes' {
				$all_packages = ['' ]
				$puppetcertpemdir = "/etc/puppetlabs/puppet/ssl/certs"
				$puppetkeypemdir = "/etc/puppetlabs/puppet/ssl/private_keys"

			
				$agent_cfg = "/etc/puppetlabs/mcollective/server.cfg"
				$client_cfg = "/etc/puppetlabs/mcollective/client.cfg"
				$agent_service = "mcollective"
				$mcol_modpath = "/opt/puppetlabs/mcollective/plugins/mcollective"
				
				$yamlfacts = "/etc/puppetlabs/mcollective/facts.yaml"
				$mcol_libdir="/opt/puppetlabs/mcollective/plugins"


				file {["/opt","/opt/puppetlabs","/opt/puppetlabs/mcollective","/opt/puppetlabs/mcollective/plugins"]:
					ensure => directory
				}
			}
		}
		default: {

			if $mcol_puppetlabs=='yes' {
				$all_packages = ['' ]
				$puppetcertpemdir = "/var/lib/puppet/ssl/certs"
				$puppetkeypemdir = "/var/lib/puppet/ssl/private_keys"

			
				$agent_cfg = "/etc/puppetlabs/mcollective/server.cfg"
				$client_cfg = "/etc/puppetlabs/mcollective/client.cfg"
				$agent_service = "mcollective"
				$mcol_modpath = "/opt/puppetlabs/mcollective/plugins/mcollective"
				
				$yamlfacts = "/etc/puppetlabs/mcollective/facts.yaml"
				$mcol_libdir="/opt/puppetlabs/mcollective/plugins"


			}
			else {
				$all_packages = [ 'mcollective' ]
				$puppetcertpemdir = "/var/lib/puppet/ssl/certs"
				$puppetkeypemdir = "/var/lib/puppet/ssl/private_keys"

			
				$agent_cfg = "/etc/mcollective/server.cfg"
				$client_cfg = "/etc/mcollective/client.cfg"
				$agent_service = "mcollective"
				$mcol_modpath = "/usr/share/mcollective/plugins/mcollective"
				
				$yamlfacts = "/etc/mcollective/facts.yaml"
				$mcol_libdir="/usr/share/mcollective/plugins"
			}

				

		}
	}

	$puppetcapem = "$puppetcertpemdir/ca.pem"
	$m_certname = $trusted[certname]

	$puppetcertpem = "$puppetcertpemdir/$m_certname.pem"
	$puppetkeypem = "$puppetkeypemdir/$m_certname.pem"




}

class mcol::activemq(
)
inherits mcol::broker

{
	package {$amq_packages:
		ensure => installed
	} ->

	file {"$amq_config":
		ensure => file,
		content => template("mcol/activemq.xml.erb"),
	} ->

	java_ks { 'puppetca:truststore':
		ensure       => latest,
		certificate  => $puppetcapem,
		target       => $brokerts,
		password     => 'puppet',
		trustcacerts => true,
		require => Package[$amq_packages],
	} ->

	java_ks { 'puppetca:keystore':
		ensure       => latest,
		certificate  => $puppetcertpem,
		private_key  => $puppetkeypem,
		target       => $brokerks,
		password     => 'puppet',
		trustcacerts => true,
		require => Package[$amq_packages],
	} ->

	service {$amq_service:
		ensure => running,
		enable => true,
		subscribe => File["$amq_config"]
	}
	
}




class mcol::rabbit(
)
inherits mcol::broker
{
	$rabbit_ssldir = "$rabbit_dir/ssl"
	$rabbit_sslkeydir = "$rabbit_ssldir/priv"
	$rabbit_cacert = "$rabbit_ssldir/ca.pem"
	$rabbit_config = "$rabbit_dir/rabbitmq.config"


	$rabbit_cert = "$rabbit_ssldir/$m_certname.pem"

	$rabbit_key = "$rabbit_sslkeydir/$m_certname.pem"


	class { 'rabbitmq':
		service_manage    => true,
		port              => 5672,
		#delete_guest_user => true,
	}
	rabbitmq_vhost { $vhost:
		ensure => present,
	}

	rabbitmq_user { "$mcoladmin":
		admin    => true,
		password => $mcoladmin_password,
	}

	rabbitmq_user_permissions { "$mcoladmin@$main_collective":
		configure_permission => '.*',
		read_permission      => '.*',
		write_permission     => '.*',
	}

	rabbitmq_user { "$mcolagent":
		admin    => false,
		password => $mcolagent_password,
	}

	rabbitmq_user_permissions { "$mcolagent@$main_collective":
		configure_permission => '.*',
		read_permission      => '.*',
		write_permission     => '^stomp-subscription-.*|amq.gen-.*|amq.default',
	}

	rabbitmq_plugin {'rabbitmq_stomp':
		ensure => present,
	}
	$collectives.each | $collective | {

		rabbitmq_exchange { "${collective}_broadcast@$main_collective":
			ensure      => present,
			user        => $mcoladmin,
			password    => $mcoladmin_password,
			type        => 'topic',
			internal    => false,
			auto_delete => false,
			durable     => true,
			arguments   => {
				hash-header => 'message-distribution-hash'
			}
		}

		rabbitmq_exchange { "${collective}_directed@$main_collective":
			ensure      => present,
			user        => $mcoladmin,
			password    => $mcoladmin_password,
			type        => 'direct',
			internal    => false,
			auto_delete => false,
			durable     => true,
			arguments   => {
				hash-header => 'message-distribution-hash'
			}
		}
	}

	file {"$rabbit_ssldir":
		ensure => directory,
		owner => $rabbit_user,
		require => Class["mcol::install_rabbit"]
	}
	file {"$rabbit_sslkeydir":
		ensure => directory,
		owner => $rabbit_user,
		require => File["$rabbit_ssldir"],
		mode => '0600',
	}


	file {"$rabbit_cacert":
		ensure => file,
		source => "file:$puppetcapem",
		owner => $rabbit_user,
		mode => '0600',
		require => File[$rabbit_ssldir],
	}

	file {"$rabbit_cert":
		ensure => file,
		source => "file:$puppetcertpem",
		owner => $rabbit_user,
		mode => '0600',
		require => File[$rabbit_ssldir],
	}


	file {"$rabbit_key":
		ensure => file,
		source => "file:$puppetkeypem",
		owner => $rabbit_user,
		mode => '0600',
		require => File[$rabbit_sslkeydir],
	}

	file {"$rabbit_config":
		ensure => file,
		content => template("mcol/rabbitmq.config.erb"),
		require => File[ $rabbit_key ]
	}




}

class mcol::broker (
	$mcoladmin = "mcoladmin",
	$mcoladmin_password = "mcoladmin",
	$mcolagent = "mcolagent",
	$mcolagent_password ="mcolagent",
	$collectives = ["mcollective"],
	$broker = 'activemq',

	$admingroups = 'admins',
	$agentgroups = 'agents',

	$main_collective = "mcollective",

	$vhost = 'mcollective',

) inherits mcol::params {

	if $broker == 'rabbit' {
		class {"mcol::rabbit":
		}
	}
	else {
		class {"mcol::activemq":
		}
	}

}


class mcol::plugins(
	$plugins
) 
inherits mcol::params
{
#	$plugins.each | $plugin | {
		file { "mcoplugin":
			ensure => 'directory',
			source => "puppet:///mcol/plugins",
			recurse => 'remote',
			path => "$mcol_modpath",
#			owner => 'vagrant',
#			group => 'vagrant',
#			mode  => '0744', # Use 0700 if it is sensitive
		}

#	}


}


class mcol::agent(
	$mcolagent = "mcolagent",
	$mcolagent_password ="mcolagent",

	$broker = 'activemq',
	$broker_server,
	$vhost = 'mcollective',

	$plugins = ['shell','puppet','service'],

	$agent = true,
	$client = false,

	$main_collective = 'mcollective',
	$collectives = ['mcollective'],

	$mcol_provider = $mcol::params::package_provider ,
)
inherits mcol::params
{

	if $mcol_puppetlabs == 'no'  {
		package { $all_packages:
			provider => $mcol_provider
		} 
	}

	class { "mcol::plugins":
		plugins => $plugins
	} ->

	if $agent {
#		package { $agent_packages: 
#		} ->

		file { $agent_cfg:
			ensure => file,
			content => template("mcol/server.cfg.erb"),
#			require => Package[ $agent_packages ]
		} ->

		service { $agent_service:
			ensure => running,
			subscribe => [File[$agent_cfg],Class["mcol::plugins"]],
		}


	}

#	if $client {
#		package { $client_packages: 
#		}
#	}

}



